(defpackage cl-http-test/tests/main
  (:use :cl
        :cl-http-test
        :rove))
(in-package :cl-http-test/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :cl-http-test)' in your Lisp.

;; please run a local server at below address
(defvar *root-host* "http://localhost:3000")

(deftest index-path-get-ok
  (testing "route path should be able to be accessed"
    (ok (cl-http-test:get-ok *root-host* "/"))))

(deftest index-path-title-is
  (testing "document title should be YES!"
    (ok (cl-http-test:title-is *root-host* "/" "YES!"))))

(deftest index-path-title-not
  (testing "document title should not be 'bear'"
    (ok (not (cl-http-test:title-is *root-host* "/" "bear")))))

(deftest index-path-content-contains
  (testing "document should contains 'pass beyond'"
    (ok (cl-http-test:content-contains *root-host* "/" "pass beyond"))))

(deftest index-path-content-contains-not
  (testing "document should not contains 'duck'"
    (ok (not (cl-http-test:content-contains *root-host* "/" "duck")))))

