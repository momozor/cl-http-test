(defpackage cl-http-test
  (:use :cl)
  (:export :get-ok
           :title-is
           :content-contains))
(in-package :cl-http-test)

(defun get-content (root-host route)
  (dex:get (format nil "~a~a" root-host route)))

(defun get-ok (root-host route)
  (eql
   (nth-value 1 (get-content root-host route))
   200))

(defun get-parsed-content (root-host route)
  (plump:parse (get-content root-host route)))

(defun title-is (root-host route title)
  (let* ((parsed (get-parsed-content root-host route))
         (title-content
          (aref (lquery:$ parsed "title" (text)) 0)))
    (string= title-content title)))

(defun content-contains (root-host route regex)
  (let* ((parsed (get-parsed-content root-host route))
         (all-content
          (aref (lquery:$ parsed (text)) 0)))
    (cl-ppcre:scan regex all-content)))
