(defsystem "cl-http-test"
  :version "0.1.0"
  :author "Momozor"
  :license "MIT"
  :depends-on ("dexador"
               "plump"
               "lquery"
               "cl-ppcre")
  :components ((:module "src"
                        :components
                        ((:file "main"))))
  :description "A http testing library for Common Lisp"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README"))
  :in-order-to ((test-op (test-op "cl-http-test/tests"))))

(defsystem "cl-http-test/tests"
  :author "Momozor"
  :license "MIT"
  :depends-on ("cl-http-test"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for cl-http-test"

  :perform (test-op (op c) (symbol-call :rove :run c)))
